import UIKit

// Задача 1
var milkmanPhrase: String = "Молоко — это полезно"
print(milkmanPhrase)


// Задача 2
var milkPrice: Double = 3


// Задача 3
milkPrice = 4.20


// Задача 4
var milkBottleCount: Int? = 20
var profit: Double = 0.0

if let notNil = milkBottleCount {
    profit = milkPrice * Double(notNil)
    print(profit)
}   else    {
    print("Нет бутылок — нет профита")
}


// Дополнительное задание со зведочкой
/* Force unwrap не стоит использовать, т.к. попытка такого развертывания действительно nil переменной приводит к runtime ошибкам, а то и к крашу приложения */


// Задача 5
var employeesList: Array? = []

employeesList = ["Марфа", "Андрей", "Иван", "Петр", "Геннадий",]


// Задача 6
var isEveryoneWorkHard: Bool = false
var workingHours: Double = 40 // На случай если кто-то на неполную ставку, и может/должен работать дробное кол-во часов

switch workingHours {
case 40...:
    isEveryoneWorkHard = true
case ..<40:
    isEveryoneWorkHard = false
default:
    isEveryoneWorkHard = true    // Возможно здесь лучше что-то вроде fatalError(), на случай ошибки в логике
}
print(isEveryoneWorkHard)
